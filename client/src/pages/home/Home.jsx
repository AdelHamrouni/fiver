import React from 'react'
import "./Home.scss"
import Featured from '../../components/featured/Featured'
import TrustedBy from '../../components/trustedBy/TrustedBy';
import Slide from '../../components/slide/Slide'
import {cards, projects} from "../../data.js";
import CatCard from '../../components/catCard/CatCard';
import ProjectCard from '../../components/projectCard/ProjectCard';

  
export default function Home() {
  return (
    < div className='home'>
      <Featured />
      <TrustedBy />
       <Slide slidesToShow={4 } arrowsScroll={4}>
       {cards.map(card => (
                <CatCard item={card} key={card.id} />
             ))} 
        </Slide>   
         <div className="features">
            <div className="container">
                <div className="item">
                <h1>A whole world of freelance talent at your fingertips</h1>
                <div className="title">
                    <img src="./img/check.png" alt='' />
                    The best for every budget
                </div>
                  <p>
                   Find high-quality services at every price point. No hourly rates,
                   just project-based pricing.
                  </p>
                  <div className="title">
              <img src="./img/check.png" alt="" />
              Quality work done quickly
            </div>
            <p>
              Find the right freelancer to begin working on your project within
              minutes.
            </p>
            <div className="title">
              <img src="./img/check.png" alt="" />
              Protected payments, every time
            </div>
            <p>
              Always know what you`ll pay upfront. Your payment isn`t released until you approve the work.
            </p>
            <div className="title">
              <img src="./img/check.png" alt="" />
              24/7 support
            </div>
            <p>
              Find high-quality services at every price point. No hourly rates,
              just project-based pricing.
            </p>
                </div>
                <div className="item">
                    <video src='./video/free.mp4' controls></video>
                </div>
            </div>
          </div>   

          <div className="features dark">
        <div className="container">
          <div className="item">
            <h1>
             <i>cherchili</i> business
            </h1>
            <h1>
              A business solution designed for teams
            </h1>
            <p>
              Upgrade to a curated experience packed with tools and benefits,
              dedicated to businesses
            </p>
            <div className="title">
              <img src="./img/check.png" alt="" />
              Connect to freelancers with proven business experience
            </div>

            <div className="title">
              <img src="./img/check.png" alt="" />
              Get matched with the perfect talent by a customer success manager
            </div>

            <div className="title">
              <img src="./img/check.png" alt="" />
              Manage teamwork and boost productivity with one powerful workspace
            </div>
            <button className='explorer'>Explore cherchili Business</button>
          </div>
          <div className="item">
            <img  src='./img/3.jpg'  alt=""
            />
          </div>
        </div>
      </div>
      <Slide slidesToShow={4} arrowsScroll={4}>
       {projects.map(card => (
                <ProjectCard item={card} key={card.id} />
             ))} 
        </Slide> 
     </div>
     
  )
}
