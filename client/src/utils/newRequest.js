import axios from "axios";

const newRequest = axios.create({
  baseURL: "http://localhost:5002/api/",
  withCredentials: true,
});

export default newRequest;