const getCurrentUser = () => {
  const user = JSON.parse(localStorage.getItem("currentUser"));
  console.log('the user in local storage is', user);
  return user;
};

export default getCurrentUser;
