import React, { useEffect, useState } from "react";
import {
  PaymentElement,
  LinkAuthenticationElement,
  useStripe,
  useElements,
} from "@stripe/react-stripe-js";

const CheckoutForm = () => {
  const stripe = useStripe();
  const elements = useElements();

  const [email, setEmail] = useState("");
  const [message, setMessage] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (!stripe) {
      return;
    }

    const clientSecret = new URLSearchParams(window.location.search).get(
      "payment_intent_client_secret"
    );

    if (!clientSecret) {
      return;
    }

    stripe.retrievePaymentIntent(clientSecret).then(({ paymentIntent }) => {
      switch (paymentIntent.status) {
        case "succeeded":
          setMessage("Paiement réussi !");
          break;
        case "processing":
          setMessage("Votre paiement est en cours de traitement.");
          break;
        case "requires_payment_method":
          setMessage("Votre paiement n'a pas réussi, veuillez réessayer.");
          break;
        default:
          setMessage("Quelque chose s'est mal passé.");
          break;
      }
    });
  }, [stripe]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!stripe || !elements) {
      // Stripe.js n'est pas encore chargé.
      // Assurez-vous de désactiver la soumission du formulaire jusqu'à ce que Stripe.js soit chargé.
      return;
    }

    setIsLoading(true);

    const { error } = await stripe.confirmPayment({
      elements,
      confirmParams: {
        // Assurez-vous de modifier ceci pour votre page de paiement terminée
        return_url: "http://localhost:5173/success",
      },
    });

    // Ce point ne sera atteint que s'il y a une erreur immédiate lors de
    // la confirmation du paiement. Sinon, votre client sera redirigé vers
    // votre `return_url`. Pour certains modes de paiement comme iDEAL, votre client sera
    // redirigé vers un site intermédiaire d'abord pour autoriser le paiement, puis
    // redirigé vers le `return_url`.
    if (error.type === "card_error" || error.type === "validation_error") {
      setMessage(error.message);
    } else {
      setMessage("Une erreur inattendue s'est produite.");
    }

    setIsLoading(false);
  };

  const optionsElementPaiement = {
    layout: "tabs",
  };

  return (
    <form id="formulaire-de-paiement" onSubmit={handleSubmit}>
      <LinkAuthenticationElement
        id="element-d-authentification-lien"
        onChange={(e) => {
          if (e.target && e.target.value) {
            setEmail(e.target.value);
          }
        }}
      />
      <PaymentElement id="element-de-paiement" options={optionsElementPaiement} />
      <button disabled={isLoading || !stripe || !elements} id="soumettre">
        <span id="texte-du-bouton">
          {isLoading ? <div className="spinner" id="spinner"></div> : "Payer maintenant"}
        </span>
      </button>
      {/* Afficher les messages d'erreur ou de réussite */}
      {message && <div id="message-de-paiement">{message}</div>}
    </form>
  );
};

export default CheckoutForm;
