import React from 'react';
import "./CatCard.scss";
import { Link } from 'react-router-dom';

function CatCard({item}) {
  return (
    <Link to={`/gigs?cat=${item.cat}`}>
    <div className='catCard'>
          <img src={item.img}  alt='' />
          <div className='flex'>
          <span className='desc' >{item.desc}</span> 
          <span className='title ' >{item.title}</span>  
          </div>   
     </div>
    </Link>
  );
}

export default CatCard;


