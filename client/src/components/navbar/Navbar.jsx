import React, { useEffect, useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import newRequest from "../../utils/newRequest";
import "./Navbar.scss";

function Navbar() {
  const [active, setActive] = useState(false);
  const [open, setOpen] = useState(false);

  const { pathname } = useLocation();

  const isActive = () => {
    window.scrollY > 0 ? setActive(true) : setActive(false);
  };

  useEffect(() => {
    window.addEventListener("scroll", isActive);
    return () => {
      window.removeEventListener("scroll", isActive);
    };
  }, []);

  const currentUser = JSON.parse(localStorage.getItem("currentUser"));

  const navigate = useNavigate();

  const handleLogout = async () => {
    try {
      await newRequest.post("/auth/logout");
      localStorage.setItem("currentUser", null);
      navigate("/");
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className={active || pathname !== "/" ? "navbar active" : "navbar"}>
      <div className="container">
        <div className="logo">
          <Link className="Link" to="/">
            <span className="text">cherchili</span>
          </Link>
          <span className="dot">.</span>
        </div>
        <div className="links">
          <span>cherchili Business</span>
          <span>Explore</span>
          <span>English</span>
          {!currentUser?.isSeller && <span>Become a Seller</span>}
          {currentUser ? (
            <div className="user" onClick={() => setOpen(!open)}>
              <img src={currentUser.img || "/img/noavatar.jpg"} alt="" />
              <span>{currentUser?.username}</span>
              {open && (
                <div className="options">
                  {currentUser.isSeller && (
                    <>
                      <Link className="Link" to="/mygigs">
                        Gigs
                      </Link>
                      <Link className="Link" to="/add">
                        Add New Gig
                      </Link>
                    </>
                  )}
                  <Link className="Link" to="/orders">
                    Orders
                  </Link>
                  <Link className="Link" to="/messages">
                    Messages
                  </Link>
                  <Link className="Link" onClick={handleLogout}>
                    Logout
                  </Link>
                </div>
              )}
            </div>
          ) : (
            <>
             <span> <Link to="/login" className="Link">Sign in</Link> </span>
              <Link className="Link" to="/register">
                <button>Join</button>
              </Link>
            </>
          )}
        </div>
      </div>
      {(active || pathname !== "/") && (
        <>
          <hr />
          <div className="menu">
            <Link className="Link menuLink" to="/">
              Graphics & Design
            </Link>
            <Link className="Link menuLink" to="/">
              Video & Animation
            </Link>
            <Link className="Link menuLink" to="/">
              Writing & Translation
            </Link>
            <Link className="Link menuLink" to="/">
              AI Services
            </Link>
            <Link className="Link menuLink" to="/">
              Digital Marketing
            </Link>
            <Link className="Link menuLink" to="/">
              Music & Audio
            </Link>
            <Link className="Link menuLink" to="/">
              Programming & Tech
            </Link>
            <Link className="Link menuLink" to="/">
              Business
            </Link>
            <Link className="Link menuLink" to="/">
              Lifestyle
            </Link>
          </div>
          
        </>
      )}
    </div>
  );
}

export default Navbar;