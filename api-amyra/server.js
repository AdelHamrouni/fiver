import express from 'express';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import userRoute from './routes/user.route.js';




import authRoute from './routes/auth.route.js';


const app = express()
dotenv.config();
mongoose.set("strictQuery",true);


async function connectToMongoDB() {
    try {
        await mongoose.connect(process.env.MONGO);
        console.log("Connecté à MongoDB");
    } catch (erreur) {
        console.log(erreur);
    }
}

app.use(express.json());

app.use("/api/auth", authRoute);
app.use("/api/users", userRoute);
//app.use("/api/gigs", gigRoute);
//app.use("/api/orders", orderRoute);
//app.use("/api/conversations", conversationRoute);
//app.use("/api/messages", messageRoute);
//app.use("/api/reviews", reviewRoute);


app.listen(5000, ()=>{
    connectToMongoDB();
    console.log("running")
});
